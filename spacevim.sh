#!/bin/bash
clear

echo 'mete la pass para elevar privilegios en donde sea necesario:  '
read -s PASS < /proc/${PPID}/fd/0

echo "Instalando spacevim..."
curl -sLf https://spacevim.org/install.sh | bash
cd ~/

echo 'Ingresá "d" si esto es Debian/Ubuntu (apt) o "a" si esto es Arch (pacman):  ' 
read NAME < /proc/${PPID}/fd/0

if [ $NAME == "d" ]; then
echo $PASS\n | sudo -s apt-get install vim neo-vim git gcc build-essential --yes
fi

if [ $NAME == "a" ]; then
echo $PASS\n | sudo -S pacman -S base-devel vim neo-vim gcc git --noconfirm
fi

git clone https://github.com/Shougo/vimproc.vim
cd vimproc.vim
make
cd lib
cp vimproc_linux64.so ~/.SpaceVim/bundle/vimproc.vim/lib/vimproc_linux64.so

cd ~
rm -R vimproc.vim

clear

echo "Fin de la instalación de spacevim"
