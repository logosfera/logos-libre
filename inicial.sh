carpeta=".logos"
ruta="$HOME/$carpeta"
usuario_actual="$USER"
clear
if [ -d "$ruta" ]; then
    source $ruta/variables.sh
    echo "cargamos variables locales"
else
    cd $HOME
    mkdir $carpeta
    cd $carpeta
    wget https://gitlab.com/logosfera/logos-libre/-/raw/master/variables.sh -O variables.sh
    source $ruta/variables.sh
    echo "descargamos variables locales"
    cd $HOME
fi

clear
echo -e "$On_Cyan $BWhite                                  Nacho  Libre                                 $Color_Off"
echo -e "$On_Green $BBlack                       === Configuración  Inicial ===                          $Color_Off"
echo -e "$Color_Off"
echo -e "\n\n\n\n"

usuario_actual="$USER"
echo -e $On_White $BBlack"El usuario actual es: $usuario_actual$Color_Off"
echo -e $On_Red $BBlack"Vamos a darle permisos de administrador."
echo -e "Seguramente te pidamos la clave de (ejem)"$Color_Off
echo -e 'Mete la pass para elevar privilegios en donde sea necesario:  '
echo -e "\n\n\n\n"
echo -e $On_Blue$UWhite"Vamos a ejecutar:"
echo -e -n $BWhite"sudo usermod -aG sudo "$usuario_actual $Color_Off
echo -e "$Color_Off"
sudo usermod -aG sudo $usuario_actual

echo -e $On_Blue$UWhite"Vamos a añadir al archivo /etc/sudoers:"
echo -e -n $BWhite $usuario_actual " ALL=(ALL:ALL) ALL " $Color_Off
echo -e "$Color_Off"
echo $usuario_actual " ALL=(ALL:ALL) ALL" >> /etc/sudoers


