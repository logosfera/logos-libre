#!/bin/bash
touch /etc/udev/rules.d/80-keychron.rules
echo 'SUBSYSTEMS=="input", ATTRS{name}=="Keychron K2", RUN+="echo 0 | tee /sys/module/hid_apple/parameters/fnmode"' > /etc/udev/rules.d/80-keychron.rules
udevadm control --reload-rules && udevadm trigger
touch /etc/modprobe.d/hid_apple.conf
echo 'options hid_apple fnmode=2' > /etc/modprobe.d/hid_apple.conf
sudo update-initramfs -u
upower --dump | grep keyboard -A 7
reboot
